% Script for varying cf
simdir = 'varcf';
mkdir(simdir);
cd(simdir);

%% Get default parameters
p = parseAsmInput();
fres = 0.5*p.cp/p.thickness;

% Define freuencies
nf = 4000;
p.f = linspace(0.5*fres, 1.5*fres, nf);

% Vary cf
nn = 200;
cfmin = 50;
cfmax = 8000;
cf = linspace(cfmin, cfmax, nn);

% Pack it up
p.filenamevars = {'cf'};

%% Do the simulations
for i = 1:nn
    p.cf = cf(i);
    startAsmSimulation(p);
end